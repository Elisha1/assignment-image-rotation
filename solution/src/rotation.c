#include "../include/rotation.h"
#include "../include/error.h"
extern struct image* rotate_left_90( struct image const* img ) {
    if (img == NULL) {
        fprintf( stderr, "No image!\n" );
        return NULL;
    }

    const size_t height = img->height;
    const size_t width = img->width;
    struct image* new_img = image_create(height, width);
    if (new_img == NULL) return NULL;
    new_img->data = malloc(sizeof(struct pixel) * new_img->height * new_img->width);
    if (new_img->data == NULL) return NULL;
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            new_img->data[height - i - 1 + j * height] = img->data[i * width + j];
        }
    }
    if (new_img == NULL || new_img->data == NULL) {
        print_err("Bad image!\n");
    }
    return (struct image*) new_img;
}
