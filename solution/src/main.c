#include "../include/image.h"
#include "../include/bmp.h"
#include "../include/error.h"
#include "../include/file.h"
#include "../include/rotation.h"
#include <stdio.h>
#include <stdlib.h>

    const char* read_errors[] = {
            [READ_INVALID_SIGNATURE] = "Invalid signature!",
            [READ_INVALID_BITS] = "Invalid pixel data!",
            [READ_INVALID_HEADER] = "Invalid header!"
    };

    const char* write_errors[] = {
            [WRITE_ERROR] = "Can't write into file."
    };

int main(int argc, char** argv ) {

    if (argc != 3) {
        print_err("Wrong number of arguments!\nInput should be:\nFirst arg for input filename;\nSecond arg for output filename.\n");
        return -1;
    }

    char *input = argv[1];
    char *output = argv[2];

    FILE* in = file_read(input);
    if (!in){
        return -1;
    }
    struct image* img = image_create(0, 0);
    enum read_status status = from_bmp(in, img);
    enum file_status closed_1 = close(in);
    if (closed_1 == ERROR){
        print_err("Can't close \"in\" file.");
        image_free(img);
        return -1;
    }
    if (status != READ_OK) {
        print_err(read_errors[status]);
        image_free(img);
        return -1;
    }

    struct image* new_img = rotate_left_90(img);
    image_free(img);

    FILE* out = file_write(output);
    if (!out){
        print_err("Can't create \"out\" file.");
        image_free(new_img);
        return -1;
    }
    enum write_status w_status = to_bmp(out, new_img);
    image_free(new_img);
    enum file_status closed_2 = close(out);
    if (closed_2 == ERROR){
        print_err("Can't close \"out\" file.");
        return -1;
    }
    if (w_status != WRITE_OK) {
        print_err(write_errors[w_status]);
        return -1;
    }
    return 0;
}
