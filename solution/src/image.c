#include "../include/image.h"
#include "../include/error.h"
#include <stdlib.h>

extern struct image* image_create( size_t width, size_t height ) {
    struct image* img = malloc(sizeof(struct image));
    img->data = NULL;
    img->width = width;
    img->height = height;
    if (img == NULL) {
        print_err("Out of memory\n");
    }
    return img;
}
extern void image_free(struct image* img) {
    if (img) {
        if (img->data) free(img->data);
        free(img);
    }
}
