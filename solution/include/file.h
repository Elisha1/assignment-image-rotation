#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <stdlib.h>

enum file_status {
    OK,
    ERROR
};
FILE* file_read(const char* pathname);
FILE* file_write(const char* pathname);
enum file_status close(FILE* file);

#endif
