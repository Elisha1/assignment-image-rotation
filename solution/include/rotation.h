#ifndef ROTATION_H
#define ROTATION_H

#include "image.h"
#include <inttypes.h> 
#include <stddef.h>
#include <stdint.h> 
#include <stdlib.h> 

struct image* rotate_left_90( struct image const* img);

#endif
